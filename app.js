$(document).ready(function() {

  // automatically show selected tab 
  // if URL includes tab identifier after #
  if (location.hash) {
    var hash = location.hash
      , hashPieces = hash.split('?')
      , activeTab = $('[href=' + hashPieces[0] + ']');
    activeTab && activeTab.tab('show');
  }

});
