Project ATMs_NearbyProject

Instructions - First of all since coding was done in PHP programming language XAMPP Apache server should be mounted so we can run application in localhost.

It's a web app that searches for nearby ATMs. Makes a list of 10 closest ATMs from user location. First thing that is asked before running on localhost is for your geolocation. After knowing geolocation using Google Places API and Google Static maps API it returnes names, adresses and link to mini map image of ATMs location with our location. Our location is marked green, while ATMs location is marked red. We have also a map with all ATMs location and our location.

One small tip, this is half finished application. 
In making - 
1) Sorting by distance with calculated distance between our location and ATMs location.
2) Making two different lists one with single currency ATMs, second with multi currency ATMs.
